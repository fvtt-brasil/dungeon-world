# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.4.0](https://gitlab.com/foundryvtt-pt-br/dungeon-world/compare/v1.3.1...v1.4.0) (2020-12-07)


### Features

* update compendiums translations ([40bc2c9](https://gitlab.com/foundryvtt-pt-br/dungeon-world/commit/40bc2c9dee1cf1921adef67220c20d33785e09e4))

### [1.3.1](https://gitlab.com/foundryvtt-pt-br/dungeon-world/compare/v1.3.0...v1.3.1) (2020-11-11)


### Bug Fixes

* **dungeonworld.basic-moves:** fix missing babele mapping ([8021a93](https://gitlab.com/foundryvtt-pt-br/dungeon-world/commit/8021a93cf86ebf202fd8fbd3470956070770464e))

## [1.3.0](https://gitlab.com/foundryvtt-pt-br/dungeon-world/compare/v1.2.0...v1.3.0) (2020-11-11)


### Features

* update compendiums and system translations ([057fca9](https://gitlab.com/foundryvtt-pt-br/dungeon-world/commit/057fca9aa51113300fac047e811c3561523fbbf3))

## [1.2.0](https://gitlab.com/foundryvtt-pt-br/dungeon-world/compare/v1.1.0...v1.2.0) (2020-11-09)


### Features

* translate compendiums ([4f9a489](https://gitlab.com/foundryvtt-pt-br/dungeon-world/commit/4f9a4899718a1ab22ad82da11a87f5192e927ad9))

## [1.1.0](https://gitlab.com/foundryvtt-pt-br/dungeon-world/compare/v1.0.1...v1.1.0) (2020-11-05)


### Features

* translate dungeonworld.basic-moves.json ([bf93e54](https://gitlab.com/foundryvtt-pt-br/dungeon-world/commit/bf93e5463fe5f787761f5dd40e500abeff571641))

### [1.0.1](https://gitlab.com/foundryvtt-pt-br/dungeon-world/compare/v1.0.0...v1.0.1) (2020-11-03)


### Bug Fixes

* **module.json:** fix wrong path to system translation file ([c4f6836](https://gitlab.com/foundryvtt-pt-br/dungeon-world/commit/c4f6836502a4f1053b0a1068af771e2baaf19f5d))

## 1.0.0 (2020-11-03)


### Features

* initial release ([d2c4013](https://gitlab.com/foundryvtt-pt-br/dungeon-world/commit/d2c40139de5f4f0275076d468db8a31435a50b46))
